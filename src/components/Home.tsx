import React from "react";
import logo from "../logo.svg";
import "../App.css"

export default function Home() {
    return(
        <div>
            <h1>Home Page</h1>
            <img src={logo} className="App-logo" alt="logo" />
        </div>
    );
}