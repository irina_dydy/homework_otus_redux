import React from "react";
import {AppBar, Grid, Toolbar} from "@mui/material";
import Button from '@mui/material/Button';
import { useHistory} from 'react-router-dom';
import {unauthorize} from '../features/authorize/authorizeSlice'
import { useAppDispatch } from '../app/hooks';
import { AuthSelect } from '../features/authorize/authorizeSlice';
import { useSelector } from 'react-redux';



export default function Header() {
    const history = useHistory();
    const dispatch = useAppDispatch();

    const authSelector = useSelector(AuthSelect);
    
    const isAutorized = authSelector.status === "authorized";

    console.log(`${history} it is history`);

    function homeRoute(){
        history.push('/');
    }

    const signUpRoute=()=>{
        history.push('/signup');
    }

    const signInRoute=()=>{
        history.push('/signin');
    }

    const logOut=()=>{
        dispatch(unauthorize());
    }

    return (
        <header>
            <AppBar>
                <Toolbar>
                    <Grid container spacing={0}>
                        <Grid item  xs={1}>
                            <Button color="inherit" onClick={homeRoute}>Home</Button>
                        </Grid>
                        <Grid item xs={8}></Grid>
                        <Grid item xs ={3} hidden={isAutorized}>
                            <div>
                                <Button color="inherit" onClick={signInRoute}>SignIn</Button>
                                <Button color="inherit" onClick={signUpRoute}>SignUp</Button>
                            </div>
                        </Grid>
                        <Grid item xs ={3} hidden={!isAutorized}>
                            <div>
                                <Button color="inherit" onClick={logOut}>Logout</Button>
                            </div>
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
        </header>
    );
}

