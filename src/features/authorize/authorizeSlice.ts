import {  createSlice } from '@reduxjs/toolkit';

export interface AuthorizeState {
    status: 'authorized' | 'unauthorized';
  }

const initialState: AuthorizeState = {
    status: 'unauthorized'
};

interface Auth{
  authorizer: AuthorizeState
}


export const authorizeSlice = createSlice({
    name: 'authorizer',
    initialState,
    // The `reducers` field lets us define reducers and generate associated actions
    reducers: {
        authorize: (state)=> {
          state.status = "authorized"
        },
        unauthorize: (state)=> {state.status = "unauthorized"}
    },
  });

  export const AuthSelect = (state: Auth) => state.authorizer;
  

  export const { authorize, unauthorize } = authorizeSlice.actions;

  export default authorizeSlice.reducer;
