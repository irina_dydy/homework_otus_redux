import React from "react";
import './App.css';
import Header from "./components/Header";
import {Route, Switch } from "react-router-dom";
import SignIn from "./components/SignIn";
import SignUp from "./components/SignUp";
import Home from "./components/Home";
import NotFound from "./components/NotFound";

import AuthorizedRoute from "./utils/AuthorizedRoute";

const App=()=> {
  return (
    <div className="App">
      <Header/>
        <div>
          <Switch>
            <Route exact path="/signin" component={SignIn}/>
            <Route exact path="/signup" component={SignUp}/>
            <AuthorizedRoute path="/" component={Home}/>
            <Route component={NotFound}/>
          </Switch>
        </div>
    </div>
  );
}

export default App;
